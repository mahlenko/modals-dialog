/**
 * Example:
 *  1) <a href="{url}?{searchParams}" data-modal="true">Open modal ajax request</a>
 *  2) <button data-dialog="true" data-url="{url}?{searchParams}">Open dialog</button>
 *  3) <button data-modal="true" data-url="#{template_id}">Open modal is template</button>
 *     <template id="modalbox" data-fullscreen="true">
 *         Modal content
 *     </template>
 *
 * Params:
 *  data-url=""             URL or #{template_id}
 *  data-fullscreen="true"  For mobile, full screen
 *  data-dialog="true"      The dialog can only be closed by clicking the button [x] or .modal-close
 *  data-method="post"      Request method, only for ajax
 *
 *  // подписаться на обновления
 *  document.addEventListener('DOMContentLoaded', function()
 *  {
 *      ModalComponent.setOptions({ logs: true });
 *      ModalComponent.registration(document);
 *  });
 *
 */
export default {
    /* true - игнорирует события, используется во время анимаций */
    eventsIgnore: false,

    /**
     *
     */
    options: {
        logs: false,
        container: 'modals-container',
        backdrop: 'modals-backdrop',

        modal_container: 'modals-container__item',
        modal: 'modal',

        loader: {
            class: 'modal-loader',
            item: '<i class="fas fa-spinner fa-spin"></i>',
        },
        close: {
            text:  '<i class="fas fa-times"></i>',
            class: 'modal-close'
        },

        animate: {
            close_error: {class: 'animate__headShake', duration: .5},
            showModal: {class: 'animate__zoomIn', duration: .15},
            hideModal: {class: 'animate__zoomOut', duration: .3}
        },
        events: {
            beforeOpen: () => {
            },
            afterOpen: ($modal) => {
            },
            beforeClose: ($modal) => {
            },
            afterClose: () => {
            },
        },
        axios: {
            options: {
                url: '',
                method: 'get',
                headers: {
                    'X-CSRF-TOKEN': document
                        .getElementsByTagName('meta')
                        .namedItem('csrf-token')
                        .getAttribute('content')
                }
            },
            events: {
                content: (container, response) => {
                    return container.innerHTML += response.data;
                },
                before: () => {
                    //
                },
                success: (container, response) => {
                    //
                },
                fail: (container, error) => {
                    return container.innerHTML = '<i class="fas fa-exclamation-triangle"></i>' +
                        error.status +
                        ': ' + error.statusText;
                },
                complete: (container) => {
                    //
                }
            }
        }
    },

    /* -------------------------------------------------------------- */
    /* Methods                                                        */

    setOptions: function (options) {
        Object.assign(this.options, options);
    },

    get: function (url, data, attributes, events, options) {
        return this._load(
            Object.assign(Object.assign(this.options.axios.options, options), {url, params: data}),
            events,
            attributes);
    },

    post: function (url, data, attributes, events, options) {
        return this._load(
            Object.assign(Object.assign(this.options.axios.options, options), {url, data, method: 'post'}),
            events,
            attributes);
    },

    put: function (url, data, attributes, events, options) {
        return this._load(
            Object.assign(Object.assign(this.options.axios.options, options), {url, data, method: 'put'}),
            events,
            attributes);
    },

    delete: function (url, data, attributes, events, options) {
        return this._load(
            Object.assign(Object.assign(this.options.axios.options, options), {url, data, method: 'delete'}),
            events,
            attributes);
    },

    patch: function (url, data, attributes, events, options) {
        return this._load(
            Object.assign(Object.assign(this.options.axios.options, options), {url, data, method: 'patch'}),
            events,
            attributes);
    },

    show: function (element) {
        let modal = document.getElementById(element.substr(1));

        if (!modal.length && modal.tagName !== 'TEMPLATE') {
            this.log('Шаблон модального окна <template></template> не найден.', false);
            return false;
        }

        /*  */
        this._show(
            this._create(modal.innerHTML.trim()),
            modal.dataset
        );

        return false;
    },

    hide: function (force) {
        /*  */
        this._hide(force);

        return false;
    },

    destroy: function ()
    {
        document.getElementsByClassName(this.options.container)[0].remove();

        if (this.getCount()) {
            this.log('Не удалось закрыть все окна', false);
        } else {
            this.log('Завершена работа модальных окон', true);
        }

        this._window_block(false);
    },

    registration: function (container) {
        this._registration_tag('a', container);
        this._registration_tag('button', container);
    },

    /* -------------------------------------------------------------- */
    /* Actions                                                        */

    _init: function () {
        var container = document.getElementsByClassName(this.options.container);

        if (container.length) {
            this.log('Используется ранее созданный контейнер');
            return container[0];
        }

        /*  */
        this._window_block(true);

        /*  */
        container = document.createElement('div')
        container.className = this.options.container

        /*  */
        let backdrop = document.createElement('div')
        backdrop.className = this.options.backdrop
        container.append(backdrop)

        /*  */
        document.body.append(container);

        this.log('Создан контейнер для модальных окон');

        return container;
    },

    _create: function (content) {
        /*  */
        this.options.events.beforeOpen()

        /*  */
        let container = document.createElement('div')
        container.classList.add(this.options.modal_container);

        /*  */
        let close = document.createElement('a')
        close.setAttribute('href', '#');
        close.classList.add(this.options.close.class);
        close.innerHTML = this.options.close.text;

        /*  */
        let modal = document.createElement('div')
        modal.classList.add(this.options.modal);
        modal.append(close);
        modal.innerHTML += content;

        /*  */
        container.append(modal)

        return container;
    },

    _animate: function (element, animate, duration, callback) {
        element.style.setProperty('--animate-duration', duration + 's');

        element.classList.add('animate__animated');
        element.classList.add(animate);

        setTimeout(() => {
            element.classList.remove(animate);
            callback ? callback(element) : true;
        }, duration * 1000);
    },

    _show: function (modal, dataAttributes) {
        var container = this._init()
        var backdrop = container.getElementsByClassName(this.options.backdrop)[0]
        var index = this.getActiveIndex()

        /*  */
        backdrop.style.zIndex = index;
        modal.style.zIndex = index;

        /*  */
        this.log('Выполняю пользовательскую функцию: beforeOpen');
        this.options.events.beforeOpen();

        /*  */
        dataAttributes.active = true;
        for (const [key, value] of Object.entries(dataAttributes)) {
            modal.dataset[key] = value;
        }

        this._animate(
            modal,
            this.options.animate.showModal.class,
            this.options.animate.showModal.duration,
            (modal) => {
                modal.classList.add('overflow');

                /*  */
                for (var i = 0; i < modal.getElementsByClassName(this.options.close.class).length; i++) {
                    let element = modal.getElementsByClassName(this.options.close.class)[i];

                    /* закрытие по нажатию на [x] */
                    element.addEventListener('mouseup', (e) => {
                        if (e.which === 1) {
                            this.hide(true);
                        }
                    });
                }
            });

        /* закрытие по нажатию на подложку */
        modal.addEventListener('mouseup', (e) => {
            if (e.which === 1 && window.getSelection().toString() === '') {
                if (e.target === this.getActive()) {
                    this.hide();
                }
            }
        });

        container.append(modal);

        if (modal.dataset.dialog === 'true') {
            this.log('Открыл диалоговое окно', true);
        } else {
            this.log('Открыл модальное окно', true);
        }

        /*  */
        this.registration(modal)

        this.log('Выполняю пользовательскую функцию: afterOpen');
        this.options.events.afterOpen(modal)
    },

    _hide: function (force) {

        this.eventsIgnore = true;

        var modal = this.getActive();
        modal.classList.remove('overflow')

        /*  */
        if (modal.dataset.dialog === 'true' && force !== true) {
            /*  */
            this.log('Диалоговое окно можно закрыть только на [x] или элемент с классом ' + this.options.close.class, false);

            this._animate(modal,
                this.options.animate.close_error.class,
                this.options.animate.close_error.duration,
            );

            this.eventsIgnore = false;

            return;
        }

        /*  */
        this.log('Выполняю пользовательскую функцию: beforeClose');
        this.options.events.beforeClose(modal);

        /*  */
        this._animate(modal,
            this.options.animate.hideModal.class,
            this.options.animate.hideModal.duration,
            (modal) => {
                if (modal.dataset.dialog === 'true') {
                    this.log('Диалоговое окно закрыто', true);
                } else {
                    this.log('Модальное окно закрыто', true);
                }

                /*  */
                modal.remove();

                if (this.getCount() === 0) {
                    this.destroy();
                }

                /*  */
                this.eventsIgnore = false;

                /*  */
                this.log('Выполняю пользовательскую функцию: afterClose');
                this.options.events.afterClose();
            });

        /*  */
        document.getElementsByClassName(this.options.backdrop)[0].style.zIndex--;
    },

    _load: function (options, events, attributes) {
        var $this = this;

        /*  */
        var $events = Object.assign(this.options.axios.events, events);

        /*  */
        var preloader = document.createElement('span');
        preloader.className = this.options.loader.class;
        preloader.innerHTML = this.options.loader.item;

        /* создаём модальное окно с прелоадером */
        var modal = this._create(preloader.outerHTML);

        /* прячем [x] в модальном окне */
        var closeButton = modal.getElementsByClassName(this.options.close.class)[0];
        closeButton.style.display = 'none';

        /* показываем прелоадер */
        this._show(modal, attributes);

        /* пользовательская функция перед отправкой запроса */
        this.log('Выполняю пользовательскую функцию: before');
        $events.before();

        /*  */
        this.log(options);

        /*  */
        axios(options)
            .then(function (response) {
                /*  */
                $this.log('Получен ответ от сервера', true);

                /* показываем [x] в модальном окне */
                closeButton.style.display = 'block';

                /* прячем loader */
                modal.getElementsByClassName($this.options.loader.class)[0].remove()

                /* добавляем html */
                $this.log('Выполняю пользовательскую функцию: content');
                $events.content(
                    modal.getElementsByClassName($this.options.modal)[0],
                    response);

                /* пользовательская функция */
                $this.log('Выполняю пользовательскую функцию: success');
                $events.success(modal.getElementsByClassName($this.options.modal)[0], response)
            })
            .catch(function (error) {
                $this.log('Произошла ошибка', false);

                $this.log('Выполняю пользовательскую функцию: fail');
                $events.fail(modal.getElementsByClassName($this.options.modal)[0], error.response);
            })
            .then(function () {
                $this.log('Выполняю пользовательскую функцию: complete');
                $events.complete(modal.getElementsByClassName($this.options.modal)[0])
            });

        return false;
    },

    _events_active: function (e) {
        if (!this.eventsIgnore && e.keyCode === 27 && this.getCount()) {
            this.log('Закрытие по нажатию Esc');
            return this.hide();
        }
    },

    _window_block(blocked) {
        if (blocked) {
            document.body.style.overflow = 'hidden';
            document.addEventListener('keydown', (e) => { this._events_active(e) }, false);
        } else {
            document.body.style.removeProperty('overflow');
        }
    },

    _registration_tag: function (tagName, container) {
        var $this = this;
        var elements = container.getElementsByTagName(tagName);

        for (var i = 0; i < elements.length; i++) {
            var element = elements[i];

            /* модальные окна */
            if (element.hasAttribute('data-modal') || element.hasAttribute('data-dialog')) {
                /* отслеживаем клик */
                element.addEventListener('click', function (e) {
                    /* ссылка на модальное окно */
                    var url = this.tagName === 'A'
                        ? this.getAttribute('href')
                        : this.dataset.url;

                    if (url.substr(0, 1) === '#') {
                        /* html шаблон */
                        $this.show(url);
                    } else {
                        /* метод запроса */
                        var method = this.dataset.method ?? 'get';

                        /*  */
                        url = new URL(url, window.location.protocol + '//' + window.location.hostname);

                        var data = {};
                        if (url.search) {
                            data = JSON.parse('{"' + decodeURI(url.search.substr(1)).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"') + '"}');
                        }

                        $this[method](url.pathname, data, this.dataset);
                        e.preventDefault();
                    }

                    return false;
                })
            }
        }
    },

    /* -------------------------------------------------------------- */
    /* Helpers                                                        */

    getCount: function () {
        var container = document.getElementsByClassName(this.options.container)
        if (!container.length) return 0;

        var modals = container[0].getElementsByClassName(this.options.modal_container)

        return modals.length;
    },

    getActive: function () {
        var container = document.getElementsByClassName(this.options.container)
        if (!container.length) return;

        var modals = container[0].getElementsByClassName(this.options.modal_container)

        if (modals.length) {
            for (var i = modals.length - 1; i >= 0; i--) {
                if (modals[i].dataset.active === 'true') {
                    return modals[i];
                }
            }
        }
    },

    getActiveIndex: function ()
    {
        var modals = document.getElementsByClassName(this.options.modal_container);
        var activeModal = this.getActive();

        for (let i = 0; i < modals.length; i++) {
            if (modals[i] === activeModal) {
                return i + 1;
            }
        }

        return 0;
    },

    log: function (message, type) {
        if (this.options.logs === false) {
            return false;
        }

        switch (type) {
            case true:
                var style = 'background: #55C25E; color: white';
                break;
            case false:
                var style = 'background: #D63851; color: white';
                break;
            default:
                var style = 'background: #0076ff; color: white';
                break;
        }

        var now = new Date();

        var date = [now.getHours(), now.getMinutes(), now.getSeconds()];
        for (const [key, value] of Object.entries(date)) {
            if (value < 10) {
                date[key] = '0' + value
            }
        }

        console.log("%c " + date.join(':') + " ", style, message);
    }
};
