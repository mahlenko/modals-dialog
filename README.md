# Модальные окна #
Реализация мульти-модальных и диалоговых окон без jQuery на чистом JS. Библиотека использует [animate.style](https://animate.style/)
для анимации появления, исчезновения и ошибок в окнах.

### Возможности ###
* Модальные окна
* Диалоговые окна (то же что и модальные, но не закрываются по клику на фон или esc - только кнопка)
* Показ окна сформированного в шаблоне. Используя тег `<template></template>`
* Загрузка окна по ссылке используя библиотеку [axios](https://github.com/axios/axios)
* Поддержка `data-` атрибутов
* Возможность открыть неограниченное число окон поверх предыдущего
* Длинные модальные окна - работают с собственным скролом, не прокручивая основную страницу
* Защита от лже-закрытия окна (когда выделяем текст в модальном окне, и опускаем клик за пределом окна, в таком случае окно - не закроется)
* Собственные колбеки, на всех этапах работы окна `beforeOpen`, `afterOpen`, `beforeClose`, `afterClose`. Для модальных окон по url доступны ещё: `before`, `content`, `success`, `fail`, `complete`.
* Автоматически подставит csrf-token при запросах к серверу. Добавьте на страницу meta тег с токеном `<meta name="csrf-token" content="@csrf">`

## Установка ##

Загрузите компонент и вспомогательные библиотеки: [animate.style](https://animate.style/#usage) и [axios](https://github.com/axios/axios)

```javascript
import ModalComponent from './app/ModalComponent';

document.addEventListener('DOMContentLoaded', function()
{
    ModalComponent.setOptions({ logs: true });
    ModalComponent.registration(document);
});
```

## Подключите `scss` ##

```scss
@import "{path_to_file}/ModalComponent";
```

По-умолчанию, компонент использует только базовые стиль, для позиционирования, стилизуйте модальное окно под себя


```css
.modals-backdrop {
	background-color: rgba(0,0,0,.6);
}

.modal {
	margin: auto; /* центрировать окно */
	/* ... */
}
```

## Опции ##

## Методы ##
